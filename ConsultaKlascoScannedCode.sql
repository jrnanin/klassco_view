-- Vista 0.01
drop table  if exists tabla_tmp;

 CREATE TEMPORARY TABLE tabla_tmp AS
-- CREATE TABLE tabla_tmp AS
(SELECT STRAIGHT_JOIN SQL_BIG_RESULT distinct
    spd.auditID,
    spd.scannedCode,
	ad.visitDate,
    ad.start as 'startaudit',
    pc.comision,
    pc.start as 'startcomision',
    pc.end as 'endcomision',
    ad.employeeID,
    ad.salePointID,
    pc.zoneID,
    ud.areaID,
    dp.distributorID
FROM
    audit_data ad
        INNER JOIN
    scanned_products_data spd ON ad.id = spd.auditID
        INNER JOIN
    products_data pd ON spd.productID = pd.id
        INNER JOIN
    products_comision pc on pd.id=pc.productID
        INNER JOIN
    user_data ud ON ad.employeeID = ud.id
    inner join 
    distibutor_products dp on pd.id=dp.productID
WHERE
    ad.replicated = 0
    -- AND YEAR(ad.start) = 2018
	-- and MONTH(ad.start) = 4
	-- AND (MONTH(ad.start) >= 1 or MONTH(ad.start) <= 4)
	 AND Year(ad.visitDate) =2018
    -- and month(ad.visitDate) =4
     and (month(ad.visitDate) >=1 or month(ad.visitDate) <=4)
    and ad.start !='0000-00-00 00:00:00'
    and ad.start !='0000-00-00T00:00:00'
    LIMIT 5000
);




SELECT
	a.zoneID,
    a.areaID,
    date(a.startaudit) as 'fecha',
	month(a.startaudit) as 'mes',
    week(a.startaudit) as 'semana',
    a.salePointID,
    a.employeeID,
    a.distributorID,
    a.scannedCode,
    a.comision
    
FROM
    tabla_tmp a
 GROUP BY  
	-- date(a.startaudit),
	-- week(a.startaudit),
    -- month(a.startaudit),
    -- a.zoneID,
    -- a.areaID,
    scannedCode,  
    -- a.comision , 
    a.employeeID
    -- a.distributorID
