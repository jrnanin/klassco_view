SELECT 
    date(ad.start) 'fecha',
    time(ad.start) 'hora',
    CONCAT(ud.name, ' ', ud.surname1) AS 'nombre',
    ud.id as 'userid',
    ud.areaID,
    spd.description,
    spd.productID,
    spd.upcCode,
    spd.klasscoCode,
    spd.family,
    spd.subfamily,
    spd.scannedCode,
    spd.price,
    pc.comision,
    ad.id as 'adid',
    sapd.id as 'sapdid',
    sapd.name
FROM
    audit_data ad
        INNER JOIN
    scanned_products_data spd ON spd.auditID = ad.id
        INNER JOIN
    user_data ud ON ud.id = ad.employeeID
        INNER JOIN
    sales_points_data sapd ON ad.salePointID = sapd.id
    inner join 
	products_comision pc on pc.productID=spd.productID
where 
	year(ad.start)>=2018
    and month(ad.start)=2
    and ad.replicated=0
limit 50    