drop table  if exists adtemp;
 CREATE TEMPORARY TABLE adtemp as (
SELECT 
	ad.id,
    ad.visitDate,
    ad.start,
    ad.employeeID,
    ad.salePointID
FROM
    audit_data ad
WHERE
    ad.replicated = 0
    -- La parte de la fecha cambiarla para que sea 
	AND YEAR(ad.visitDate) = 2018
	AND MONTH(ad.visitDate) = 4
    and day(ad.visitDate) =10
    and ad.start !='0000-00-00 00:00:00'
    and ad.start !='0000-00-00T00:00:00'
    );

    
 select * from adtemp ad group by ad.id;  
-- select * from adtemp ad inner join scanned_products_data spd on ad.id=spd.auditID  ;
-- select ad.id,count(*) from adtemp ad inner join scanned_products_data spd on ad.id=spd.auditID group by ad.id;
-- select spd.auditID,count(*) from adtemp ad inner join scanned_products_data spd on ad.id=spd.auditID group by spd.auditID;    
-- select count(*) from (select spd.auditID,count(*) from adtemp ad inner join scanned_products_data spd on ad.id=spd.auditID group by spd.auditID) a;
drop table  if exists pd_pc_temp;
 CREATE TEMPORARY TABLE pd_pc_temp as (
SELECT STRAIGHT_JOIN
    pc.productID,
    pc.zoneID,
    pc.comision
FROM
    products_data pd
        INNER JOIN
    products_comision pc ON pd.id = pc.productID
where
date(now()) between pc.start and pc.end     
 );   
 
select * from pd_pc_temp;
-- En esta consulta se puede ver registros duplicados
-- select * from scanned_products_data spd right join adtemp ad on spd.auditID=ad.id where spd.id is not null and spd.scannedCode='KAFFPCA131101181849988'; -- ver por que se repite el scannedCode en teoria la consulta esta bien solo que si se tienen registros duplicados   
-- select codi,conteo from (select spd.scannedCode as 'codi',count(spd.scannedCode) as 'conteo' from scanned_products_data spd right join adtemp ad on spd.auditID=ad.id where spd.id is not null group by spd.scannedCode ) a where conteo>1; -- ver por que se repite el scannedCode en teoria la consulta esta bien solo que si se tienen registros duplicados
--
drop table  if exists spd_adt_temp;
CREATE TEMPORARY TABLE spd_adt_temp as (
SELECT STRAIGHT_JOIN
    spd.productID, spd.auditID, spd.scannedCode
FROM
    scanned_products_data spd
        RIGHT JOIN
    adtemp ad ON spd.auditID = ad.id
WHERE
    spd.id IS NOT NULL
GROUP BY spd.scannedCode
 );  
 
select * from spd_adt_temp; 
 
drop table  if exists ud_adt_temp;
CREATE TEMPORARY TABLE ud_adt_temp as (
 SELECT STRAIGHT_JOIN
    ud.id AS 'ud_id',
    ad.id AS 'ad_id',
    ad.salePointID AS 'sp_id'
FROM
    adtemp ad
        INNER JOIN
    user_data ud ON ad.employeeId = ud.id
 );
 
 
 
 
 
drop table  if exists pd_pc_dp_temp;
CREATE TEMPORARY TABLE pd_pc_dp_temp as ( 
-- select * from pd_pc_temp pdpct left join distibutor_products dp on pdpct.productID=dp.productID -- para esta consulta se puede ver la incongruencia de registros  que no tienen distribuidor el detalle es como llega el produto
select pdpct.*,dp.distributorID,dp.distributorCode from pd_pc_temp pdpct inner join distibutor_products dp on pdpct.productID=dp.productID
 );
 
 
-- select * from adtemp a inner join pd_pc_temp b on a.id   ,spd_adt_temp c,ud_adt_temp d,pd_pc_dp_temp e;
select * from pd_pc_dp_temp

 /*
SELECT 
    ad.id,
    ad.visitDate,
    ad.start AS 'startaudit',
    ad.employeeID,
    ad.salePointID,
    spd.auditID,
    spd.scannedCode,
    dp.distributorID
FROM
    adtemp ad
        INNER JOIN
    scanned_products_data spd ON ad.id = spd.auditID
        INNER JOIN
    products_data pd ON spd.productID = pd.id
        INNER JOIN
    products_comision pc on pd.id=pc.productID
        INNER JOIN
    user_data ud ON ad.employeeID = ud.id
    inner join 
    distibutor_products dp on pd.id=dp.productID
where spd.scannedCode='KAFSSCA140901181845347' and  year(pc.start)>=2018;



select * from products_comision;
select * from products_comision group by start;
select * from products_comision pc left join products_data pd on pc.productID=pd.id
where date(now()) between pc.start and pc.end     ;


SELECT 
    *
FROM
    products_data pd
        INNER JOIN
    products_comision pc ON pd.id = pc.productID
    where pd.id =1
    and year(start)>=2018;*/