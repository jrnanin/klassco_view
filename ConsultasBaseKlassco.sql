select count(*), 'ud' from user_data union				
select count(*), 'ad' from audit_data union				
select count(*), 'spd' from scanned_products_data union				
select count(*), 'pd' from products_data union				
select count(*), 'pc' from products_comision union				
select count(*), 'spd' from sales_points_data;				
				
select * from user_data;				
select * from distributors_data;				
select * from distibutor_products;				
select * from audit_data;				
select distinct end from products_comision ;				
select * from scanned_products_data a;				
select * from sales_points_data;				
select * from distibutor_products where distributorID=4;				
select * from distributors_data;
select * from products_data;
-- select * from scanned_products_data a;
-- group by scannedCode;



select * from audit_data ad ;
	-- where ad.visitDate>='2018-02-01'
    -- and ad.start>='2018-02-01';          


           
-- explain
SELECT STRAIGHT_JOIN SQL_BIG_RESULT distinct
    spd.auditID,
    spd.scannedCode,
	ad.visitDate,
    ad.start as 'startaudit',
    pc.comision,
    pc.start as 'startcomision',
    pc.end as 'endcomision',
    ad.employeeID,
    ad.salePointID,
    pc.zoneID,
    ud.areaID,
    dp.distributorID
FROM
    audit_data ad
        INNER JOIN
    scanned_products_data spd ON ad.id = spd.auditID
        INNER JOIN
    products_data pd ON spd.productID = pd.id
        INNER JOIN
    products_comision pc on pd.id=pc.productID
        INNER JOIN
    user_data ud ON ad.employeeID = ud.id
    inner join 
    distibutor_products dp on pd.id=dp.productID
WHERE
    ad.replicated = 0
     AND Year(ad.visitDate) =2018
     and month(ad.visitDate) =4;
	-- AND ad.visitDate >= '2018-04-01'
	-- AND ad.start >= '2018-04-01'
	-- and dp.distributorID=4;			
                
                
                
select start,count(*) from products_comision group by start;				
select * from products_comision where start>='2018-02-01';				



-- indexado de la tabla scanned_products_data sobre el campo auditID que nos permite una mayor ejecucion en la primera vista a trabajar
create index auditIDIndex on scanned_products_data(auditID);




select year(start), month(start),week(start),count(*) from audit_data where replicated=0 group by year(start), month(start),week(start);
select year(start), month(start),count(*) from audit_data where replicated=0 group by year(start), month(start);
select year(start), count(*) from audit_data where replicated=0 group by year(start);